//pages: contiene todo el html, van a ser HTML ELEMENTS

export class HomePage extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `

        <style>

            .main {
                font-size: 1.5rem;
                height:90vh;   
                
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;

            }

            
            .main__link { 
                color: var(--color-dark);
                font-size: 2.8rem;
                font-weight: bold;
                padding: 1rem;
                
           } 

            .main__link:hover { 
                color: var(--color-relaxed);
                font-weight: bold;
                
           } 
           
        
        </style>
            
        
        <main class="main">
        <img alr="imagen">
            <h2 class="main__title">Let's hit today's training session!</h2>
            <a class="main__link" href="/my-training">Go</a>
        </main>

            
        `;
    }

}

customElements.define('home-page', HomePage);