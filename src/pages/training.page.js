import "./../components/training.component"

export class TrainingPage extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `

        <style>
        

            .main {
                font-size: 1.5rem;
                /*height:85vh;   */

                display: flex;
                flex-direction: column;
                padding: 2rem;
            }

            .main__text-wrapper {
                padding-bottom: 3rem;
                
            }
            .main__title {
                padding-bottom: 1rem;
            }

            .main__input {
                height: 2.3rem;
                padding: 0.5rem;
                border-radius: 4px;
            }
            

            .footer {
                display: flex;
                height: 4vh;
                justify-content: center;
                align-items: flex-end;
            }

            .footer__link {
                color: var(--color-dark);
            }
        
        </style>

        
        <main class="main">
            
        
            <div class="main__text-wrapper">
                <h2 class="main__title second-title">This is my training page</h2>
            </div>
            

            <training-posts></training-posts>
        
        </main>
        <footer class="footer">
            <span >developed by <a class="footer__link" href="">mZubeldia</a></span>
        </footer>
            
            
        `;
    }

}

customElements.define('training-page', TrainingPage);

// <input disabled class="main__input" type="text" min="2" placeholder="type an exercise">
