import { html, css, LitElement } from "lit";

class ExercisesUI extends LitElement {

    static get properties() {
        return {
            exercises: {
                type: Array
            }
        }
    }

    constructor() {
        super();
        this.favoritesCards = [];

    }

    connectedCallback() {
        super.connectedCallback();

    }

    static styles = css`

    ol,
    ul {
      list-style: none;
    }
    
    li {
      list-style-type: none;
    }

    .exercises-list{
        display: grid;

        grid-template-columns: repeat(1, 1fr);
        gap: 20px;
        padding: 1rem;
    }

    @media (min-width: 760px) {
        .exercises-list {
            grid-template-columns: repeat(3, 1fr);
      }
    }
    @media (min-width: 1200px) {
        .exercises-list {
            grid-template-columns: repeat(4, 1fr);
    }}

    .main__article:hover {
        background-color: #af367e;
    }
    .favorite__card {
        padding: 0.5rem;
        border: 2px solid black;
        width: 15rem;
        align-self: center;

        justify-self: center;

        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;

        cursor: pointer;
        border-radius: 6px;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

        margin-bottom: 1rem;

        background-color: #af367e;
    }
    
    .main__article {
        padding: 0.5rem;
        border: 2px solid black;
        width: 15rem;
        align-self: center;

        justify-self: center;

        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;

        background-color: var(--color-happy);

        cursor: pointer;
        border-radius: 6px;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

        margin-bottom: 1rem;

    }
    .main__article-favorite {
        padding: 0.5rem;
        border: 2px solid black;
        width: 15rem;
        align-self: center;

        justify-self: center;

        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;

        background-color: var(--color-happy);

        border-radius: 6px;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

        margin-bottom: 1rem;
    }

    .article__title {
        text-transform: capitalize;
        text-align: center;
    }

    .article__main {
        display: flex;
        flex-direction: column;
        height: 5rem;
        justify-content: space-between;
    }
    .article__image {
        height: 220px;
    }

    .favorites-list {
        display: grid;

        grid-template-columns: repeat(1, 1fr);
        gap: 20px;
        padding: 1rem;
    }

    @media (min-width: 760px) {
        .favorites-list {
            grid-template-columns: repeat(3, 1fr);
      }
    }
    @media (min-width: 1200px) {
        .favorites-list {
            grid-template-columns: repeat(4, 1fr);
            }
        }
    }
    
    `;

    handleFavorites(ev) {
        const cardId = ev.currentTarget.id;
        const updateFavorite = ev.currentTarget;
        //encuentra el elemento clicado, usando el id
        const showFavorites = this.favoritesCards.find((cardClicked) => {
            return cardClicked.id === cardId

        });

        //si mi array de favoritos está vacío, búscalo en el array global
        //y pushealo el clicado, usando el id
        if (showFavorites == null) {

            const selectedElement = this.exercises.find((cardClicked) => {
                return cardClicked.id === cardId

            });

            this.favoritesCards.push(selectedElement);
            this.requestUpdate();
            updateFavorite.classList.add("favorite__card");
            updateFavorite.classList.remove("main__article");

        } else {
            //si el elemento ya está en mi array de favoritos, elimínalo
            
            let index = this.favoritesCards.indexOf(showFavorites);
            this.favoritesCards.splice(index, 1);
            this.requestUpdate();
            updateFavorite.classList.add("main__article");
            updateFavorite.classList.remove("favorite__card");
        }
    }

    render() {
        return html`
        <section class="favorites__section">
        <h3>Favorites</h3>
            <ul class="favorites-list">
            ${this.favoritesCards.map((favorite) =>
            html`
                <li class="favorite-element">
                    <article class="main__article-favorite" id="${favorite.id}">
                        <header class="article__header">
                            <h4 class="article__title third-title"> ${favorite.name}</h3>
                        </header>
                            <main>
                                <p class="article__text">Equipment: ${favorite.equipment}</p>
                                <img class="article__image" src="${favorite.gifUrl}" alt="exercise image">
                            </main>

                    </article>
                </li>
                
                `)}


            </ul>
        </section>



        <section class="exercises_section">
            <h3>All results</h3>

            <ul class="exercises-list">
            
                ${this.exercises && this.exercises.map((exercise) =>
                html`
                    
                    <li class="exercise-element" id="${exercise.id}" >

                        <article class="main__article" @click="${this.handleFavorites}" id="${exercise.id}">
                            <header class="article__header">
                                <h4 class="article__title third-title">${exercise.name}</h3>
                            </header>

                            <main class="article__main">
                                <p class="article__text">Body part: ${exercise.bodyPart}</p>
                            </main>

                        </article>

                    </li>

                `   )}
            </ul>


        </section>




        `;
    }



}

customElements.define("exercises-ui", ExercisesUI);
