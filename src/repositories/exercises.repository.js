import axios from "axios";
//aquí solo llamo y devuelvo los datos que me pasa la api
//aquí puedo tener n métodos, todos los que necesite para conectar con la api

//no hace falta testear esto
export class ExercisesRepository {

    async getBodyPart() {
        const config = {
            headers: {
                'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com',
                'X-RapidAPI-Key': '6297627988mshad45219ee7a1f1ep12df8fjsn2796db9cd596'
            }

        }
        return await (
            await axios.get("https://exercisedb.p.rapidapi.com/exercises/name/curl", config)
        ).data;

    }

}