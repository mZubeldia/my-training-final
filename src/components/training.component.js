import { html, LitElement } from "lit";
import { AllExercisesUseCase } from "../usecases/all-exercises.usecase";
import "./../ui/exercises.ui";


//este componente llama al método que llama a la api
//no pinta los elementos
export class TrainingComponent extends LitElement {

    static get properties() {
        return {

            exercises: {
                type: Array,
                state: true
            }
            
        }
    }

    async connectedCallback() {
        super.connectedCallback();

        const allExercisesUseCase = new AllExercisesUseCase();
        this.exercises = await allExercisesUseCase.execute();
        
    }

    render() {
        return html `
        <exercises-ui .exercises="${this.exercises}"></exercises-ui>
        `
    }

}

customElements.define('training-posts', TrainingComponent);