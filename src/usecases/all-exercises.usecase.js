import { ExercisesRepository } from "../repositories/exercises.repository";

//casos de uso que necesito en mi aplicación
//cada caso de uso tendrá un único método

export class AllExercisesUseCase {

    async execute() {
        const repositoryDb = new ExercisesRepository();
        const exercises = await repositoryDb.getBodyPart();
        //const arrExercises = Object.entries(exercises);
        return exercises;
    }

}