import { Router } from '@vaadin/router';
import './pages/home.page';
import './pages/training.page';

const outlet = document.querySelector('#outlet');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', component: 'home-page' },
    { path: '/my-training', component: 'training-page' },
    { path: '(.*)', redirect: '/' }
])

