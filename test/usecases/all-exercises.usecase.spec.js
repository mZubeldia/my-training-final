import { ExercisesRepository } from "../../src/repositories/exercises.repository";
import { AllExercisesUseCase } from "../../src/usecases/all-exercises.usecase";
import { EXERCISES } from "../fixtures/exercises";

jest.mock('../../src/repositories/exercises.repository');

describe('All exercises use Case', () => {

    beforeEach(() => {
        ExercisesRepository.mockClear();
    })

    it('should execute correct', async () => {

        ExercisesRepository.mockImplementation(() => {
            return {
                getBodyPart: () => {
                    return EXERCISES;
                }
            }
        })

        const useCase = new AllExercisesUseCase();
        const exercises = await useCase.execute();

        expect(exercises).toHaveLength(EXERCISES.length);

    })

})